package DsAlgotest;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DsAlgopages.DSAlgoBrowser;
import DsAlgopages.DSQueue;
import DsAlgopages.DSlogin;

public class Queue extends DSAlgoBrowser {

	
	  @BeforeTest public void Setup() { 
		  Browser_open(); 
		  lwaittime();
	  
	  }
	 

	@Test(dataProvider = "UserRegistedLogin")
	public void QueueDS(String user, String password1) {
		DSlogin login = new DSlogin();
		DSQueue Qu = new DSQueue();

		login.GetStarted();
		mwaittime();
		login.signin();
		mwaittime();
		login.Userpass(user, password1);
		login.login1();
		mwaittime();

		Qu.Queue();
		Qu.Queueclick();
		mwaittime();
		Qu.QueueImplemantation();
		mwaittime();
		Qu.QueueSteps();
		Browser2_Back();
		Qu.QueueCollections();
		mwaittime();
		Qu.QueueSteps();
		mwaittime();
		Browser2_Back();
		Qu.QueueArray();
		Qu.QueueSteps();
		Browser2_Back();
		Qu.QueueOperations();
		Qu.QueueSteps();
		Browser2_Back();
		// Qu.Graph();
		login.signout();

	}
	
	@AfterTest
	public void close()
	{
		Browser_close();
	}
}

package DsAlgotest;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DsAlgopages.DSAlgoBrowser;
import DsAlgopages.DSlogin;

public class Login extends DSAlgoBrowser {

	public DSlogin login = new DSlogin();

	@BeforeTest
	public void browser() {
		Browser_open();
	}

	@Test(dataProvider = "LoginData")
	public void DSlogin(String user, String password1) {
		login.GetStarted();
		login.signin();
		login.Userpass(user, password1);
		login.login1();
		login.userverification();
        login.signout();
	}

	@AfterClass
	public void Logout() {
		//
		//
		// mwaittime();

	}

	/*
	 * @AfterTest public void close() { Browser_close(); }
	 */

}

package Assignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class Browser {
	public static WebDriver driver1;
	public static String baseurl="https://dsportalapp.herokuapp.com/";


@BeforeTest
	public static void Browser_open()
   {
	System.setProperty("webdriver.chrome.driver", "C:/Sunandha/drivers/chromedriver.exe");
	WebDriver driver1 = new ChromeDriver();
	driver1.get(baseurl);
   }

@Test(priority =1)
   public static void Browser_Maximize() throws InterruptedException
   {
    driver1.manage().window().maximize();
    Thread.sleep(300);
    driver1.findElement(By.xpath("//*[contains(text( ),'Get Started')]")).click();
    Thread.sleep(500);
    driver1.navigate().back();
    Thread.sleep(400);
    driver1.navigate().forward();
    String title=driver1.getTitle();
    System.out.println(title+",Website is displayed");
    driver1.findElement(By.xpath("//*[contains(text( ),'Get Started')]")).click();

	//Thread.sleep(500);
   }

/*@Test
   public static void Browser_Navigate()
   {
	//homepage.getstarted.click();
	driver1.navigate().back();
	driver1.navigate().forward();
   }

@AfterTest
     public static void browser_open()
     {
	  driver1.close();
     }*/
}

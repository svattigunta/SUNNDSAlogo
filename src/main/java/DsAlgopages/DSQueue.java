package DsAlgopages;

import java.time.Duration;

import org.openqa.selenium.By;

public class DSQueue extends DSlogin {

	public String str="print(100)";

	By Queue=By.xpath("//a[@href='queue']");
	By QueueImplemantation=By.xpath("//a[@href='implementation-lists' and text()='Implementation of Queue in Python']");
	By QueueCollections=By.xpath("//a[@href='implementation-collections' and text()='Implementation using collections.deque']");
	By QueueArray=By.xpath("//a[@href='Implementation-array' and text()='Implementation using array']");
	By QueueOperations=By.xpath("//a[@href='QueueOp' and text()='Queue Operations']");
	By QueueTry=By.xpath("//a[@href='/tryEditor' and text()='Try here>>>']");
	By Queueprint=By.xpath("//textarea[@autocorrect='off']");
	By QueueRun=By.xpath("//button[@type='button' and text()='Run']");
	By Dataoutputverification = By.xpath("//pre[@id='output']");
	By Dropdown=By.xpath("//a[@href='#'and text()='Data Structures']");
	By Graph=By.xpath("//a[@href='/graph'and text()='Graph']");
	
	public void Queueclick()
	{
		driver1.findElement(Queue).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
	}


	public void Queue()
	{
		//driver1.findElement(Queue).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
		System.out.println("=========================");
		System.out.println("DataStructure:Queue");
		System.out.println("=========================");
	}

	public void QueueImplemantation()
	{
		driver1.findElement(QueueImplemantation).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("1.QueueImplemantation");
	}

	public void QueueCollections()
	{
		driver1.findElement(QueueCollections).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("2.QueueCollections");
	}

	public void QueueArray()
	{
	   driver1.findElement(QueueArray).click();
	   driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
	   System.out.println("3.QueueArray");
	}

	public void QueueOperations()
	{
		driver1.findElement(QueueOperations).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("4.QueueOperations");
	}

	public void QueueSteps()
	{
			driver1.findElement(QueueTry).click();
			driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
			driver1.findElement(Queueprint).sendKeys(str);
			System.out.println("String:"+str);
			driver1.findElement(QueueRun).click();
			driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
			if (driver1.findElements(Dataoutputverification).size() != 0) {
				String outputtext = driver1.findElement(Dataoutputverification).getText();
				System.out.println(outputtext);
				String msg = str.replaceAll("[()]", "");
				String Input = msg.replaceAll("print", "");
				// System.out.println(output);
				System.out.println(Input + " and " + outputtext + " are same");
			} else {
				System.out.println("Input is not given");
			}
			driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));

	}

	public void Graph()
	{
		driver1.findElement(Dropdown).click();
		driver1.findElement(Graph).click();

	}

}

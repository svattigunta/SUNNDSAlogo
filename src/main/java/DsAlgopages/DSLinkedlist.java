package DsAlgopages;

import java.time.Duration;

import org.openqa.selenium.By;

public class DSLinkedlist extends DSlogin {
	public String str="print(180)";

	By Linkedlist=By.xpath("//a[@href='linked-list']");
	By LinkedIntroduction=By.xpath("//a[@href='introduction' and text()='Introduction']");
	By LinkedCreation=By.xpath("//a[@href='creating-linked-list' and text()='Creating Linked LIst']");
	By LinkedTypes=By.xpath("//a[@href='types-of-linked-list' and text()='Types of Linked List']");
	By LinkedImplement=By.xpath("//a[@href='implement-linked-list-in-python' and text()='Implement Linked List in Python']");
	By LinkedTraversal=By.xpath("//a[@href='traversal' and text()='Traversal']");
	By LinkedInsertion=By.xpath("//a[@href='insertion-in-linked-list' and text()='Insertion']");
	By LinkedDeletion=By.xpath("//a[@href='deletion-in-linked-list' and text()='Deletion']");
	By LinkedTry=By.xpath("//a[@href='/tryEditor' and text()='Try here>>>']");
	By Linkedprint=By.xpath("//textarea[@autocorrect='off']");
	By LinkedRun=By.xpath("//button[@type='button' and text()='Run']");
	By Dataoutputverification = By.xpath("//pre[@id='output']");
	By Dropdown=By.xpath("//a[@href='#'and text()='Data Structures']");
	By Queue=By.xpath("//a[@href='/queue'and text()='Queue']");
	
	

	public void Linkedlist()
	{
		//driver1.findElement(Linkedlist).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
		System.out.println("=========================");
		System.out.println("DataStructure:Linkedlist");
		System.out.println("=========================");
	}
	
	public void Linkedlistclick()
	{
		driver1.findElement(Linkedlist).click();
		
	}

	public void LinkedIntroduction()
	{
		driver1.findElement(LinkedIntroduction).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
		System.out.println("1.LinkedIntroduction");
	}

	public void LinkedCreation()
	{
		driver1.findElement(LinkedCreation).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
		System.out.println("2.LinkedCreation");
	}

	public void LinkedTypes()
	{
	   driver1.findElement(LinkedTypes).click();
	   driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
	   System.out.println("3.LinkedTypes");
	}

	public void LinkedImplement()
	{
		driver1.findElement(LinkedImplement).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("4.LinkedImplement");
	}

	public void LinkedTraversal()
	{
		driver1.findElement(LinkedTraversal).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("5.LinkedTraversal");
	}
	public void LinkedInsertion()
	{
		driver1.findElement(LinkedInsertion).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("6.LinkedInsertion");
	}
	public void LinkedDeletion()
	{
		driver1.findElement(LinkedDeletion).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("7.LinkedDeletion");
	}

	public void LinkedSteps()
	{
			driver1.findElement(LinkedTry).click();
			driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
			driver1.findElement(Linkedprint).sendKeys(str);
			System.out.println("String:"+str);
			driver1.findElement(LinkedRun).click();
			driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(1000));
			if (driver1.findElements(Dataoutputverification).size() != 0) {
					String outputtext = driver1.findElement(Dataoutputverification).getText();
					System.out.println(outputtext);
					String msg = str.replaceAll("[()]", "");
					String Input = msg.replaceAll("print", "");
					// System.out.println(output);
					System.out.println(Input + " and " + outputtext + " are same");
				} else {
					System.out.println("Input is not given");
				}
				driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
			}

	public void Queue()
	{
		driver1.findElement(Dropdown).click();
		driver1.findElement(Queue).click();

	}


}

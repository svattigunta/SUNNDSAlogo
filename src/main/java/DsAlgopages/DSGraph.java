package DsAlgopages;

import java.time.Duration;

import org.openqa.selenium.By;


public class DSGraph extends DSAlgoBrowser {
	//public WebElement G;
	public String str="print(40)";
	//By Graphs=By.xpath("//a[@class='list-group-item']");
	By Graph = By.xpath("//a[@href='graph']");
	By GraphElement = By.xpath("//a[@href='graph' and text()='Graph']");
	By GraphRepresentations = By.xpath("//a[@href='graph-representations' and text()='Graph Representations']");
	By GraphTry = By.xpath("//a[@href='/tryEditor' and text()='Try here>>>']");
	By Graphprint = By.xpath("//textarea[@autocorrect='off']");
	By GraphRun = By.xpath("//button[@type='button' and text()='Run']");
	By Dataoutputverification = By.xpath("//pre[@id='output']");

	public void Dgraph()
	{
		//driver1.findElement(Graph).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
		System.out.println("=========================");
		System.out.println("DataStructure:Graph");
		System.out.println("=========================");
	}

	public void GraphElement()
	{
		driver1.findElement(GraphElement).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(80));
		System.out.println("1.Graph");
	}

	/*
	 * public void GraphTry() { driver1.findElement(GraphTry).click();
	 * driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(80));
	 * System.out.println("DataStructure:Graph"); }
	 *
	 * public void Graphprint() { driver1.findElement(Graphprint).sendKeys(str);
	 * System.out.println("DataStructure:Graph");
	 *
	 * }
	 *
	 * public void GraphRun() {
	 *
	 * driver1.findElement(GraphRun).click();
	 * driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(80)); }
	 */

	public void GraphRepresentations()
	{

		driver1.findElement(GraphRepresentations).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(80));
		System.out.println("2.GraphRepresentations");
	}


	public void GraphSteps()
	{
			driver1.findElement(GraphTry).click();
			driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(80));
			driver1.findElement(Graphprint).sendKeys(str);
			System.out.println("String:"+str);
			driver1.findElement(GraphRun).click();
			driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(80));
			if (driver1.findElements(Dataoutputverification).size() != 0) {
				String outputtext = driver1.findElement(Dataoutputverification).getText();
				System.out.println(outputtext);
				String msg = str.replaceAll("[()]", "");
				String Input = msg.replaceAll("print", "");
				// System.out.println(output);
				System.out.println(Input + " and " + outputtext + " are same");
			} else {
				System.out.println("Input is not given");
			}


	}
}
